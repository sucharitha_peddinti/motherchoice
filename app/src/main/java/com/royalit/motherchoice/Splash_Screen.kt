package com.royalit.motherchoice

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager

class Splash_Screen : Activity() {
    lateinit var sharedPreferences: SharedPreferences
    var isLogined: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )


        Handler().postDelayed({

            sharedPreferences = getSharedPreferences("loginprefs", Context.MODE_PRIVATE)

//            image.visibility = View.VISIBLE
//            val anim = ValueAnimator.ofFloat(2f, 2.5f)
//            anim.duration = 4000
//            anim.addUpdateListener { animation ->
//                image.scaleX = (animation.animatedValue as Float)
//                image.scaleY = (animation.animatedValue as Float)
//            }
//            anim.repeatCount = 1
//            anim.repeatMode = ValueAnimator.REVERSE
//            anim.start()
//            image.startAnimation(AnimationUtils.loadAnimation(this, R.anim.zoomin))

            isLogined = sharedPreferences.getBoolean("islogin", false)
            if (isLogined) {
                val intent = Intent(this, HomeScreen::class.java)
                startActivity(intent)
                finish()
            } else {
                val intent = Intent(this, First_Screen::class.java)
                startActivity(intent)
                finish()
            }


        }, 3000)
    }
}