package com.royalit.motherchoice.adapter

import android.content.Context
import android.graphics.Paint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.royalit.motherchoice.R
import com.royalit.motherchoice.databinding.SubproductAdapterBinding
import com.royalit.motherchoice.models.Category_subResponse
import com.royalit.motherchoice.roomdb.CartItems

class SubProductList_Adapter(
    val context: Context,
    var languageList: ArrayList<Category_subResponse>,
    var cartData: List<CartItems>?,
    var click: ProductItemClick?


) : RecyclerView.Adapter<SubProductList_Adapter.ViewHolder>() {


    // create an inner class with name ViewHolder
    //It takes a view argument, in which pass the generated class of single_item.xml
    // ie SingleItemBinding and in the RecyclerView.ViewHolder(binding.root) pass it like this
    inner class ViewHolder(val binding: SubproductAdapterBinding) :
        RecyclerView.ViewHolder(binding.root)

    // inside the onCreateViewHolder inflate the view of SingleItemBinding
    // and return new ViewHolder object containing this layout
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = SubproductAdapterBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHolder(binding)
    }


    companion object;

    // bind the items with each item of the list languageList which than will be
    // shown in recycler view
    // to keep it simple we are not setting any image data to view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        with(holder) {
            with(languageList[position]) {
                // set name of the language from the list
//                binding.brndsTitleText.text = languageList.get(position).prodcut_name
//                binding.brandNameText.text = languageList.get(position).prodcut_desc

//                var data: Category_subResponse = languageList.get(position)
                //check user cart qty
                for (j in cartData!!.indices) {
                    if (cartData!!.get(j).getProduct_id()
                            .toInt() == (languageList.get(position).products_id.toInt())
                    ) {
                        holder.binding.qtyBtn.text = "" + cartData!![j].getCartQty()

                        holder.binding.addtocartBtn.text = "Update"
                        Log.e("addtocart qty", "" + cartData?.get(j)?.getCartQty())

                    } else {
                        holder.binding.addtocartBtn.text = "Add"
                    }
                }

                Glide.with(context).load(languageList.get(position).product_image)
                    .error(R.drawable.ic_launcher_background)
                    .transform(CenterCrop(), RoundedCorners(10))

                    .into(holder.binding.subproductImage)

                binding.subproductName.text = "" + languageList.get(position).product_name
//                binding.subproductDesc.text = "" + languageList.get(position).product_title
                binding.offerPrice.text = "\u20A8" + languageList.get(position).offer_price
                binding.salePrice.text = "\u20A8" + languageList.get(position).sales_price


                binding.salePrice.paintFlags =
                    binding.salePrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG


                if (languageList.get(position).stock.toInt() == 0) {
                    binding.outofstockBtn.visibility = View.VISIBLE
                    binding.addLayout.visibility = View.GONE
                } else {
                    binding.outofstockBtn.visibility = View.GONE
                    binding.addLayout.visibility = View.VISIBLE
                }
                Log.e("category_image", "" + languageList.get(position).product_image)
                holder.binding.subproductImage.setOnClickListener { v ->
                    click!!.onProductItemClick(
                        languageList.get(
                            position
                        )
                    )
                }
                holder.binding.subproductName.setOnClickListener { v ->
                    click!!.onProductItemClick(
                        languageList.get(
                            position
                        )
                    )
                }


                val cartQty = intArrayOf(holder.binding.qtyBtn.text.toString().toInt())
                Log.e("item_cart_qty==>", "" + cartQty[0])
                binding.plusBtn.setOnClickListener {

                    val carstQty1: String = holder.binding.qtyBtn.text.toString()
                    Log.e("item_cart_stock==>", "" + languageList.get(position).stock)
                    Log.e("item_cart_cart==>", "" + carstQty1)

                    if (languageList.get(position).stock == carstQty1) {

                        Toast.makeText(
                            context,
                            "Stock Limit only " + languageList.get(position).stock,
                            Toast.LENGTH_LONG
                        ).show()

                    } else {
                        Log.e("item_cart_==>", "" + cartQty[0])
                        Log.e("item_cart_stock==>", "" + languageList.get(position).stock)

                        cartQty[0]++
                        holder.binding.qtyBtn.text = "" + cartQty.get(0)
                    }

                }

                holder.binding.minusBtn.setOnClickListener { v ->
                    if (cartQty[0] > 0) {
                        cartQty[0]--
                    }
                    holder.binding.qtyBtn.text = "" + cartQty[0]
                }

                holder.binding.addtocartBtn.setOnClickListener {
                    try {


                        val carstQty: String = holder.binding.qtyBtn.text.toString()

                        if (carstQty == "0") {
                            Toast.makeText(context, "Select quantity..", Toast.LENGTH_LONG)
                                .show()
                        } else {
                            click?.onAddToCartClicked(languageList.get(position), carstQty)
                            Log.e("data", "" + languageList.get(position))
                            Log.e("carstQty", carstQty)
                        }
                    } catch (e: NullPointerException) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }

    interface ProductItemClick {
        fun onProductItemClick(languageList: Category_subResponse?)
        fun onAddToCartClicked(languageList: Category_subResponse?, cartQty: String?)
    }

    // return the size of languageList
    override fun getItemCount(): Int {
        return languageList.size
    }
}