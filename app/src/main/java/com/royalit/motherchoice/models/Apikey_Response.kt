package com.erepairs.app.models

import com.google.gson.annotations.SerializedName

data class Apikey_Response(

    @SerializedName("api_key") val api_key: String
)