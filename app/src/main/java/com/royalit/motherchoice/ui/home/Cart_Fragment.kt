package com.royalit.motherchoice.ui.home

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.room.Room
import com.erepairs.app.adapter.Cart_Adapter
import com.erepairs.app.api.Api
import com.royalit.motherchoice.R
import com.royalit.motherchoice.api.APIClient
import com.royalit.motherchoice.databinding.CartScreenBinding
import com.royalit.motherchoice.models.Placeorder_ListResponse
import com.royalit.motherchoice.roomdb.AppConstants
import com.royalit.motherchoice.roomdb.CartItems
import com.royalit.motherchoice.roomdb.CartViewModel
import com.royalit.motherchoice.roomdb.MotherChoiceDB
import com.royalit.motherchoice.utils.NetWorkConection
import com.royalit.motherchoice.utils.Utilities
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class Cart_Fragment : Fragment(), Cart_Adapter.ProductItemClick {
    lateinit var usersubcategory_id_: String

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: CartScreenBinding? = null
    lateinit var cart_adapter: Cart_Adapter
    var cartData: List<CartItems> = ArrayList<CartItems>()
    lateinit var motherChoiceDB: MotherChoiceDB
    var viewModel: CartViewModel? = null
    var cartItemsList: List<CartItems> = ArrayList()
    lateinit var root: View

    lateinit var productIDStr: String
    lateinit var productQtyStr: String
    lateinit var address: String
    lateinit var username: String

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    lateinit var sharedPreferences: SharedPreferences
    lateinit var category_id: String
    lateinit var customerid: String
    var productIDSB: StringBuilder? = null
    var quantitySB: StringBuilder? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(
                this,
                ViewModelProvider.NewInstanceFactory()
            ).get(HomeViewModel::class.java)

        _binding = CartScreenBinding.inflate(inflater, container, false)
        root = binding.root
        sharedPreferences =
            requireContext().getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )

        category_id = sharedPreferences.getString("categoryid", "")!!
        customerid = sharedPreferences.getString("userid", "").toString()
        address = sharedPreferences.getString("address", "").toString()
        username = sharedPreferences.getString("username", "").toString()

        Log.e("cust", customerid)
        Log.e("address", address)
        motherChoiceDB =
            Room.databaseBuilder(activity as Activity, MotherChoiceDB::class.java, "mother-choice")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries().build()
//        motherChoiceDB = MotherChoiceDB.getInstance(activity as Activity)


        viewModel = CartViewModel(activity as Activity)
        viewModel!!.cartData()

        val cartVM = CartViewModel(activity as Activity, false)
        cartVM.cartData()
        cartVM.getCartItems.observe(requireActivity(), { cartItems -> cartData = cartItems })

        binding.addressTVID.text = "" + address
        binding.userNameTVID.text = "" + username


        binding.btnPlaceOrder.setOnClickListener {

            postOrder()
        }
        try {


            viewModel!!.getCartItems.observe(requireActivity()) { cartItems ->
                cartItemsList = cartItems
                if (cartItems.size > 0) {
                    binding.itemCountTVID.text = "" + cartItems.size.toString() + " Items"
                    getTotalPrice(cartItemsList)
                    binding.mainRLID.visibility = View.VISIBLE
                    binding.noDataTVID.visibility = View.GONE

                    try {

                        val adapter = Cart_Adapter(
                            activity as Activity,
                            cartItems as ArrayList<CartItems>, cartData, this
                        )
                        binding.cartRCID.adapter = adapter

                    } catch (e: java.lang.NullPointerException) {
                        e.printStackTrace()
                    }
                } else {
                    binding.mainRLID.visibility = View.GONE
                    binding.noDataTVID.visibility = View.VISIBLE
                }
            }
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
        return root
    }

    @SuppressLint("SetTextI18n")
    private fun getTotalPrice(cartItemsList: List<CartItems>) {
        try {
            var sum = 0.0
            for (i in cartItemsList.indices) {
                sum += cartItemsList[i].getPrice().toDouble() * cartItemsList[i].getCartQty()
                    .toDouble()

                val productsids = cartItemsList[i].product_id
                var usercategoryselectedList_id = ArrayList<String>()

                quantitySB = StringBuilder()
                productIDSB = StringBuilder()

                for (eachstring in cartItemsList) {
                    productIDSB?.append(eachstring.getProduct_id())?.append("##")
                    quantitySB?.append(eachstring.getCartQty())?.append("##")
                }

                productIDStr = productIDSB.toString()
                productQtyStr = quantitySB.toString()


                if (productIDStr.length > 0) productIDStr =
                    productIDStr.substring(0, productIDStr.length - 2)


                if (productQtyStr.length > 0) productQtyStr =
                    productQtyStr.substring(0, productQtyStr.length - 2)


                Log.e("quantity==>", "" + productIDStr)
                Log.e("ids==>", "" + productQtyStr)
                usercategoryselectedList_id.add(
                    cartItemsList[i].product_id
                )


                usersubcategory_id_ = usercategoryselectedList_id.toString().trim()

                    .replace("[", "")  //remove the right bracket
                    .replace(" ", "").trim()  //remove the commas
                    .replace(",", "#").trim()  //remove the commas
                    .replace("]", "")  //remove the left bracket
                    .trim()
                Log.e("prduct id", "" + usercategoryselectedList_id)
                Log.e("usersubcategory_id_", "" + usersubcategory_id_)



                Log.e("product-id", "" + productsids)
            }
            binding.totalPriceTVID.text = "\u20b9 $sum"
        } catch (e: NumberFormatException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding
    }

    override fun onProductItemClick(itemsData: CartItems?) {

        ProductinDetails_Fragment().getData(itemsData?.product_id.toString(), true)!!

        val navController =
            Navigation.findNavController(
                context as Activity,
                R.id.nav_host_fragment_content_home_screen
            )
        navController.navigate(R.id.nav_product_details)

        val editor = sharedPreferences.edit()
        editor.putString("subcatid", itemsData?.product_id)
        editor.clear()

    }

    override fun onAddToCartClicked(itemsData: CartItems?, cartQty: String?) {
        val items = CartItems(
            0,
            "0",
            Utilities.getDeviceID(activity),
            AppConstants.PRODUCTS_CART,
            itemsData!!.itemName,
            itemsData.itemImage,
            cartQty,
            java.lang.String.valueOf(itemsData.offer_price),
            java.lang.String.valueOf(itemsData.offer_price),
            itemsData.product_id.toString(),itemsData.stock
        )
        val viewModel = CartViewModel(activity)
        viewModel.insert(items)
        Toast.makeText(activity, "Item added to cart successfully", Toast.LENGTH_LONG).show()
    }

    private fun postOrder() {

        try {


            if (NetWorkConection.isNEtworkConnected(context as Activity)) {

                //Set the Adapter to the RecyclerView//

                val addnotes = binding.cartnotes.text.toString()

                var apiServices = APIClient.client.create(Api::class.java)

                val call =
                    apiServices.getplaceordersave(
                        getString(R.string.api_key),
                        customerid, productIDStr, productQtyStr, addnotes
                    )
                binding.progressbar.visibility = View.VISIBLE

                call.enqueue(object : Callback<Placeorder_ListResponse> {
                    override fun onResponse(
                        call: Call<Placeorder_ListResponse>,
                        response: Response<Placeorder_ListResponse>
                    ) {

                        Log.e(ContentValues.TAG, response.toString())
                        binding.progressbar.visibility = View.GONE

                        try {
                            if (response.isSuccessful) {

                                //Set the Adapter to the RecyclerView//

                                val navController =
                                    Navigation.findNavController(
                                        context as Activity,
                                        R.id.nav_host_fragment_content_home_screen
                                    )
                                navController.navigate(R.id.navigation_sucess)
                                val viewModel = CartViewModel(activity as Activity)
                                viewModel.delete(cartItemsList[0], true, true)

                                val prefs = sharedPreferences.edit()
                                prefs.putString(
                                    "Orderid",
                                    response.body()?.response?.order_id.toString()
                                )
                                prefs.putString(
                                    "orderamount",
                                    response.body()?.response?.amount.toString()
                                )
                                prefs.commit()

                                Toast.makeText(
                                    context,
                                    "Order Placed Sucessfully",
                                    Toast.LENGTH_LONG
                                ).show()
                            } else {


                            }
                        } catch (e: NullPointerException) {
                            e.printStackTrace()
                        } catch (e: TypeCastException) {
                            e.printStackTrace()
                        }


                    }


                    override fun onFailure(call: Call<Placeorder_ListResponse>, t: Throwable) {
                        Log.e(ContentValues.TAG, t.toString())
                        Toast.makeText(
                            context,
                            "Something went wrong",
                            Toast.LENGTH_LONG
                        ).show()

                        binding.progressbar.visibility = View.GONE

                    }
                })


            } else {
                Toast.makeText(context, "Please Check your internet", Toast.LENGTH_LONG).show()

            }
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }


}