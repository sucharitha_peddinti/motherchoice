package com.royalit.motherchoice.ui.home

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.erepairs.app.api.Api
import com.royalit.motherchoice.R
import com.royalit.motherchoice.adapter.CategoryList_Adapter
import com.royalit.motherchoice.adapter.ProductList_Adapter
import com.royalit.motherchoice.api.APIClient
import com.royalit.motherchoice.databinding.FragmentHomeBinding
import com.royalit.motherchoice.models.Category_ListResponse
import com.royalit.motherchoice.models.Category_Response
import com.royalit.motherchoice.models.Product_ListResponse
import com.royalit.motherchoice.models.Product_Response
import com.royalit.motherchoice.roomdb.AppConstants
import com.royalit.motherchoice.roomdb.CartItems
import com.royalit.motherchoice.roomdb.CartViewModel
import com.royalit.motherchoice.roomdb.MotherChoiceDB
import com.royalit.motherchoice.utils.NetWorkConection
import com.royalit.motherchoice.utils.Utilities
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class HomeFragment : Fragment(), ProductList_Adapter.ProductItemClick {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null
    lateinit var categoryadapter: CategoryList_Adapter
    lateinit var productadapter: ProductList_Adapter
    lateinit var root: View
    var cartData: List<CartItems> = ArrayList<CartItems>()
    lateinit var sharedPreferences: SharedPreferences

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding
    lateinit var motherChoiceDB: MotherChoiceDB

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(
                this,
                ViewModelProvider.NewInstanceFactory()
            ).get(HomeViewModel::class.java)



        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        root = binding?.root!!

        sharedPreferences =
            requireContext().getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )
        motherChoiceDB =
            Room.databaseBuilder(activity as Activity, MotherChoiceDB::class.java, "mother-choice")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries().build()
//        motherChoiceDB = MotherChoiceDB.getInstance(activity as Activity)

        val cartVM = CartViewModel(activity, false)
        cartVM.cartData()
        cartVM.getCartItems.observe(requireActivity()) { cartItems -> cartData = cartItems }
        val viewModel = CartViewModel(activity as Activity)
        viewModel.cartCount()


        getCategories()
        getProductsList()

        binding?.viewall!!.setOnClickListener {
            val navController =
                Navigation.findNavController(
                    context as Activity,
                    R.id.nav_host_fragment_content_home_screen
                )
            navController.navigate(R.id.navigation_viewallproducts)

        }

        try {


            val viewModel = CartViewModel(activity as Activity)
            viewModel.cartCount()
            viewModel.cartCount.observe(requireActivity()) { cartItems ->
                if (cartItems.size > 0) {
                    binding!!.cartnumber.visibility = View.VISIBLE
                    binding!!.cvOneLogin.visibility = View.VISIBLE
                    binding?.carttext!!.visibility = View.VISIBLE
                    binding?.rlOneLogin!!.visibility = View.VISIBLE
                    binding?.cartnumber!!.text = "" + cartItems.size
                } else {
                    binding?.cartnumber!!.visibility = View.GONE
                    binding?.cvOneLogin!!.visibility = View.GONE
                    binding?.carttext!!.visibility = View.GONE
                    binding?.rlOneLogin!!.visibility = View.GONE
                }

            }

        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
        return root
    }

    private fun getCategories() {

        if (NetWorkConection.isNEtworkConnected(activity as Activity)) {

            //Set the Adapter to the RecyclerView//


            var apiServices = APIClient.client.create(Api::class.java)

            val call =
                apiServices.getcategoruesList(getString(R.string.api_key))

            binding?.homeprogress!!.visibility = View.VISIBLE
            call.enqueue(object : Callback<Category_ListResponse> {
                @SuppressLint("WrongConstant")
                override fun onResponse(
                    call: Call<Category_ListResponse>,
                    response: Response<Category_ListResponse>
                ) {

                    Log.e(ContentValues.TAG, response.toString())
                    binding?.homeprogress!!.visibility = View.GONE

                    if (response.isSuccessful) {

                        try {


                            val listOfcategories = response.body()?.response

                            //Set the Adapter to the RecyclerView//


                            val selectedserviceslist =
                                response.body()?.response!!

                            activity?.let {
                                categoryadapter =
                                    CategoryList_Adapter(
                                        activity as Activity,
                                        selectedserviceslist as ArrayList<Category_Response>
                                    )

                                binding?.categoryList!!.adapter =
                                    categoryadapter




                                categoryadapter.notifyDataSetChanged()
                                val layoutManager = GridLayoutManager(
                                    activity as Activity,
                                    3,
                                    RecyclerView.VERTICAL,
                                    false
                                )
                                binding?.categoryList!!.layoutManager = layoutManager

                                binding?.categoryList!!.setItemViewCacheSize(selectedserviceslist.size)
                            }
                        } catch (e: java.lang.NullPointerException) {
                            e.printStackTrace()
                        }

                    }


                }

                override fun onFailure(call: Call<Category_ListResponse>, t: Throwable) {
                    Log.e(ContentValues.TAG, t.toString())
                    binding?.homeprogress!!.visibility = View.GONE

                }

            })


        } else {

            Toast.makeText(
                activity as Activity,
                "Please Check your internet",
                Toast.LENGTH_LONG
            ).show()
        }

    }


    private fun getProductsList() {
        try {


            if (NetWorkConection.isNEtworkConnected(activity as Activity)) {

                //Set the Adapter to the RecyclerView//


                var apiServices = APIClient.client.create(Api::class.java)

                val call =
                    apiServices.getproductsList(getString(R.string.api_key))

                binding?.homeprogress!!.visibility = View.VISIBLE
                call.enqueue(object : Callback<Product_ListResponse> {
                    @SuppressLint("WrongConstant")
                    override fun onResponse(
                        call: Call<Product_ListResponse>,
                        response: Response<Product_ListResponse>
                    ) {

                        Log.e(ContentValues.TAG, response.toString())
                        binding!!.homeprogress.visibility = View.GONE

                        if (response.isSuccessful) {

                            try {


                                val listOfcategories = response.body()?.response

                                //Set the Adapter to the RecyclerView//


                                val selectedserviceslist =
                                    response.body()?.response!!


                                productadapter =
                                    ProductList_Adapter(
                                        activity as Activity,

                                        selectedserviceslist as ArrayList<Product_Response>,
                                        cartData,this@HomeFragment
                                    )
                                binding?.productsList!!.adapter =
                                    productadapter

                                productadapter.notifyDataSetChanged()
                                val layoutManager = LinearLayoutManager(
                                    activity as Activity,

                                    RecyclerView.HORIZONTAL,
                                    false
                                )
                                binding?.productsList!!.layoutManager = layoutManager

                            } catch (e: java.lang.NullPointerException) {
                                e.printStackTrace()
                            }

                        }


                    }

                    override fun onFailure(call: Call<Product_ListResponse>, t: Throwable) {
                        Log.e(ContentValues.TAG, t.toString())
                        binding?.homeprogress!!.visibility = View.GONE

                    }


                })


            } else {

                Toast.makeText(
                    activity as Activity,
                    "Please Check your internet",
                    Toast.LENGTH_LONG
                ).show()
            }
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding
    }

    override fun onProductItemClick(itemsData: Product_Response?) {



    }

    override fun onAddToCartClicked(itemsData: Product_Response?, cartQty: String?) {
        val items = CartItems(
            0,
            "0",
            Utilities.getDeviceID(activity),
            AppConstants.PRODUCTS_CART,
            itemsData?.product_name,
            itemsData?.product_image,
            cartQty,
            java.lang.String.valueOf(itemsData?.offer_price),
            java.lang.String.valueOf(itemsData?.offer_price),
            itemsData?.products_id.toString(),itemsData?.stock
        )
        val viewModel = CartViewModel(activity)
        viewModel.insert(items)
        Toast.makeText(context, "Item added to cart successfully", Toast.LENGTH_LONG).show()
    }


}