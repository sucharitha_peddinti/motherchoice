package com.royalit.motherchoice.ui.home

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.erepairs.app.api.Api
import com.royalit.motherchoice.R
import com.royalit.motherchoice.adapter.ViewAllProductList_Adapter
import com.royalit.motherchoice.api.APIClient
import com.royalit.motherchoice.databinding.ProductsScreenBinding
import com.royalit.motherchoice.models.Product_ListResponse
import com.royalit.motherchoice.models.Product_Response
import com.royalit.motherchoice.roomdb.AppConstants
import com.royalit.motherchoice.roomdb.CartItems
import com.royalit.motherchoice.roomdb.CartViewModel
import com.royalit.motherchoice.roomdb.MotherChoiceDB
import com.royalit.motherchoice.utils.NetWorkConection
import com.royalit.motherchoice.utils.Utilities
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class ViewAllProducts_Fragment : Fragment(), ViewAllProductList_Adapter.ProductItemClick {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: ProductsScreenBinding? = null
    lateinit var subproduct_adapter: ViewAllProductList_Adapter
    var cartData: List<CartItems> = ArrayList<CartItems>()
    lateinit var motherChoiceDB: MotherChoiceDB

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    lateinit var sharedPreferences: SharedPreferences
    lateinit var category_id: String
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(
                this,
                ViewModelProvider.NewInstanceFactory()
            ).get(HomeViewModel::class.java)

        _binding = ProductsScreenBinding.inflate(inflater, container, false)
        val root: View = binding.root
        sharedPreferences =
            requireContext().getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )

        category_id = sharedPreferences.getString("categoryid", "")!!
        motherChoiceDB =
            Room.databaseBuilder(activity as Activity, MotherChoiceDB::class.java, "mother-choice")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries().build()
        motherChoiceDB = MotherChoiceDB.getInstance(activity as Activity)

        val cartVM = CartViewModel(activity, false)
        cartVM.cartData()
        cartVM.getCartItems.observe(requireActivity()) { cartItems -> cartData = cartItems }
        getviewallProductsList()


        val viewModel = CartViewModel(activity as Activity)
        viewModel.cartCount()
        viewModel.cartCount.observe(requireActivity()) { cartItems ->
            if (cartItems.size > 0) {
                binding.cartcount.visibility = View.VISIBLE
                binding.cvOneLogin.visibility = View.VISIBLE
                binding.carttext.visibility = View.VISIBLE
                binding.rlOneLogin.visibility = View.VISIBLE
                binding.cartcount.text = "" + cartItems.size
                binding.carttext.text = "Item in your basket"
            } else {
                binding.cartcount.visibility = View.GONE
                binding.cvOneLogin.visibility = View.GONE
                binding.carttext.visibility = View.GONE
                binding.rlOneLogin.visibility = View.GONE
            }

        }

        return root
    }

    private fun getviewallProductsList() {

        if (NetWorkConection.isNEtworkConnected(activity as Activity)) {

            //Set the Adapter to the RecyclerView//


            var apiServices = APIClient.client.create(Api::class.java)

            val call =
                apiServices.getproductsList(getString(R.string.api_key))

            binding.subproductprogress.visibility = View.VISIBLE
            call.enqueue(object : Callback<Product_ListResponse> {
                @SuppressLint("WrongConstant")
                override fun onResponse(
                    call: Call<Product_ListResponse>,
                    response: Response<Product_ListResponse>
                ) {

                    Log.e(ContentValues.TAG, response.toString())
                    binding.subproductprogress.visibility = View.GONE

                    if (response.isSuccessful) {

                        try {


                            val listOfcategories = response.body()?.response

                            //Set the Adapter to the RecyclerView//


                            val selectedserviceslist =
                                response.body()?.response!!

                            subproduct_adapter =
                                ViewAllProductList_Adapter(
                                    activity as Activity,

                                    selectedserviceslist as ArrayList<Product_Response>,
                                    cartData, this@ViewAllProducts_Fragment
                                )


                            binding.subproductlist.adapter =
                                subproduct_adapter

                            subproduct_adapter.notifyDataSetChanged()
                            val layoutManager = GridLayoutManager(
                                activity as Activity,
                                2,
                                RecyclerView.VERTICAL,
                                false
                            )
                            binding.subproductlist.layoutManager = layoutManager

                        } catch (e: java.lang.NullPointerException) {
                            e.printStackTrace()
                        }

                    }


                }

                override fun onFailure(call: Call<Product_ListResponse>, t: Throwable) {
                    Log.e(ContentValues.TAG, t.toString())
                    binding.subproductprogress.visibility = View.GONE

                }

            })


        } else {

            Toast.makeText(
                activity as Activity,
                "Please Check your internet",
                Toast.LENGTH_LONG
            ).show()
        }

    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding
    }


    override fun onProductItemClick(itemsData: Product_Response?) {

        //ProductinDetails_Fragment().getData(itemsData?.products_id.toString(), true)!!

    }

    override fun onAddToCartClicked(itemsData: Product_Response, cartQty: String?) {
        val items = CartItems(
            0,
            "0",
            Utilities.getDeviceID(activity),
            AppConstants.PRODUCTS_CART,
            itemsData.product_name,
            itemsData.product_image,
            cartQty,
            java.lang.String.valueOf(itemsData.offer_price),
            java.lang.String.valueOf(itemsData.offer_price),
            itemsData.products_id.toString(),itemsData.stock
        )
        val viewModel = CartViewModel(activity)
        viewModel.insert(items)
        Toast.makeText(activity, "Item added to cart successfully", Toast.LENGTH_LONG).show()
    }
}